package ir.exam.otaghakquiz.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import ir.exam.otaghakquiz.R
import ir.exam.otaghakquiz.databinding.GetImageDialogBinding

class SelectHowGetImageDialog(val mainActivity: MainActivity) : DialogFragment() {

    lateinit var binding : GetImageDialogBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.TransparentDialog)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.get_image_dialog,container, false)
        binding.dialog = this
        return binding.root
    }

    fun gallery(){
        dismiss()
        mainActivity.getPictureFromGallery()
    }

    fun camera(){
        dismiss()
        mainActivity.getWriteStoragePermissin()
    }

}
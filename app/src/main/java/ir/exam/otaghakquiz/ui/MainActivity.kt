package ir.exam.otaghakquiz.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import ir.exam.otaghakquiz.BuildConfig
import ir.exam.otaghakquiz.R
import ir.exam.otaghakquiz.adapter.GalleryAdapter
import ir.exam.otaghakquiz.databinding.ActivityMainBinding
import ir.exam.otaghakquiz.db.AddressFile
import ir.exam.otaghakquiz.viewmodel.MainViewmodel
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random


class MainActivity : AppCompatActivity() {

    var currentPhotoPath = ""
    private lateinit var requesPermissionLuncher : ActivityResultLauncher<String>

    private lateinit var activityResultLuncher : ActivityResultLauncher<Intent>
    private val binding : ActivityMainBinding by lazy {
        DataBindingUtil.setContentView(this , R.layout.activity_main)
    }
    lateinit var viewModel : MainViewmodel

    private val adapter : GalleryAdapter by lazy {
        GalleryAdapter(viewModel)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.activity = this
        viewModel = ViewModelProvider(this)[MainViewmodel::class.java]
        recyclerview()
        observer()
        requesPermissionLuncher =registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                isGranted: Boolean ->
            if (isGranted) {
            } else {

            }
        }

        activityResultLuncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                if (data!= null){
                    var addressFile = AddressFile(data.data.toString())
                    viewModel.insert(addressFile)

                }else{
                    val image = File(currentPhotoPath)
                    var addressFile = AddressFile(Uri.fromFile(image).toString())
                    viewModel.insert(addressFile)
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()
        getReadStoragePermissin()
    }

    private fun recyclerview(){
        binding.mainActivityGalleryRv.apply {
            layoutManager = GridLayoutManager(this@MainActivity,2, GridLayoutManager.VERTICAL,false)
            adapter = this@MainActivity.adapter
        }
    }

    private fun observer(){
        viewModel.imageUrlList.observe(this , {
            if (it.isEmpty())
                galleryIsEmpty()
            else {
                galleryIsNotEmpty()
                adapter.getImageUrlList(it)
            }
        })
    }


    private fun requestCameraPermission(){
        when{
            ContextCompat.checkSelfPermission(this , Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED  -> {
                getCamera()
            }
            ActivityCompat.shouldShowRequestPermissionRationale(this , Manifest.permission.CAMERA)
            -> {
                requesPermissionLuncher.launch(Manifest.permission.CAMERA)

            }
            else -> {
                requesPermissionLuncher.launch(Manifest.permission.CAMERA)
            }
        }
    }

     fun getWriteStoragePermissin(){
        when{
            ContextCompat.checkSelfPermission(this , Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED  -> {
                requestCameraPermission()
            }

            ActivityCompat.shouldShowRequestPermissionRationale(this , Manifest.permission.WRITE_EXTERNAL_STORAGE)
            -> {
                requesPermissionLuncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)

            }
            else -> {
                requesPermissionLuncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }
    }

    fun getReadStoragePermissin(){
        when{
            ContextCompat.checkSelfPermission(this , Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED  -> {
                viewModel.getImageUrls()

            }

            ActivityCompat.shouldShowRequestPermissionRationale(this , Manifest.permission.READ_EXTERNAL_STORAGE)
            -> {
                requesPermissionLuncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)

            }
            else -> {
                requesPermissionLuncher.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        }
    }

    fun getPictureFromGallery(){
        val getPictureFromIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        activityResultLuncher.launch(getPictureFromIntent)

    }

    private fun getCamera() {
        var output : File
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            @SuppressLint("SimpleDateFormat") val timeStamp =
                SimpleDateFormat("yyyyMMdd_HHmmss").format(
                    Date()
                )
            var fileName = "JPEG_${timeStamp}_"
            var dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            output = File.createTempFile(fileName,".jpg",dir)
            if (output != null) {
                val photoURI: Uri = if (Build.VERSION.SDK_INT >= 24) {
                    FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, output)
                } else {
                    Uri.fromFile(output)
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT , photoURI)
                currentPhotoPath = output.absolutePath

                activityResultLuncher.launch(takePictureIntent)
            }

        } catch (e: IOException) {
            val i = 0;
        }
    }



    fun showDialog() {
        val dialog = SelectHowGetImageDialog(this)
        dialog.show(supportFragmentManager , " jjjjj")
    }

    fun galleryIsEmpty(){
        binding.mainActivityGalleryRv.visibility = View.GONE
        binding.mainActivityGalleryEmptyTv.visibility = View.VISIBLE
    }

    fun galleryIsNotEmpty(){
        binding.mainActivityGalleryRv.visibility = View.VISIBLE
        binding.mainActivityGalleryEmptyTv.visibility = View.GONE
    }
}
package ir.exam.otaghakquiz.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import ir.exam.otaghakquiz.db.AddressFile
import ir.exam.otaghakquiz.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewmodel(application: Application) : AndroidViewModel(application) {
    private var repository: Repository = Repository(application.applicationContext)
    var imageUrlList = MutableLiveData<List<AddressFile>>()

    fun insert(addressFile: AddressFile){
        viewModelScope.launch(Dispatchers.IO){
            repository.insert(addressFile)
            getImageUrls()
        }
    }

    fun delete(addressFile: AddressFile){
        viewModelScope.launch(Dispatchers.IO){
            repository.delete(addressFile)
            getImageUrls()
        }
    }

    fun getImageUrls() {
        viewModelScope.launch(Dispatchers.IO) {
            imageUrlList.postValue(repository.getImagesUrlList())
        }


    }
}
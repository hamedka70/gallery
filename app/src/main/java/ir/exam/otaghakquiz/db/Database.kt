package ir.exam.otaghakquiz.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [AddressFile::class] , version = 1 , exportSchema = false)
abstract class GalleryDatabase : RoomDatabase()  {


    abstract fun fileAddressDao() : FileAddressDao

    companion object{
        const val DATABASE_NAME = "gallery_db"
        @Volatile
        private var INSTANCE : GalleryDatabase ? =null
        fun getGalleryDatabase(context:Context) : GalleryDatabase ? {
            if(INSTANCE == null ){
                synchronized(GalleryDatabase::class.java){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,GalleryDatabase::class.java, DATABASE_NAME).build()
                }
            }
            return INSTANCE
        }
    }
}
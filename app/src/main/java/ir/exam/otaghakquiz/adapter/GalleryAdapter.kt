package ir.exam.otaghakquiz.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.exam.otaghakquiz.R
import ir.exam.otaghakquiz.databinding.GalleryItemLayoutBinding
import ir.exam.otaghakquiz.db.AddressFile
import ir.exam.otaghakquiz.viewmodel.MainViewmodel

class GalleryAdapter(val mainViewmodel: MainViewmodel) : RecyclerView.Adapter<GalleryAdapter.ItemViewHolder>() {
    private var imageList = ArrayList<AddressFile>()
    private lateinit var context: Context
    private lateinit var binding : GalleryItemLayoutBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.gallery_item_layout, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(imageList[position])
    }

    override fun getItemCount(): Int {
        return imageList.size
    }


    inner class ItemViewHolder(private val galleryItemLayoutBinding: GalleryItemLayoutBinding) : RecyclerView.ViewHolder(galleryItemLayoutBinding.root){
            fun bind(addressFile: AddressFile){
                galleryItemLayoutBinding.viewmodel = mainViewmodel
                galleryItemLayoutBinding.model = addressFile
                galleryItemLayoutBinding.executePendingBindings()

            }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getImageUrlList(list: List<AddressFile>){
        imageList.clear()
        imageList.addAll(list)
        notifyDataSetChanged()
    }
}
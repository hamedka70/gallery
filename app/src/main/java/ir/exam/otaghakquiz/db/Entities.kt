package ir.exam.otaghakquiz.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "File_Address")
data class AddressFile(val address:String){
    @PrimaryKey(autoGenerate = true) var id=0
}
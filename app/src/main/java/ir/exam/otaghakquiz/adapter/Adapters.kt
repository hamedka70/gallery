package ir.exam.otaghakquiz.adapter

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.signature.ObjectKey

@BindingAdapter("bind:srcInit")
fun imageItemBinding(imageview : View , addressFile: String){
        Glide.with(imageview.context)
                .load(addressFile)
                .centerCrop()
                .signature(ObjectKey(System.currentTimeMillis()))
                .into(imageview as ImageView)
}


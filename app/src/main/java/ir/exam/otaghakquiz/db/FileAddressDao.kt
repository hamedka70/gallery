package ir.exam.otaghakquiz.db

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface FileAddressDao {

    @Insert
    suspend fun insert(addressFile: AddressFile)

    @Delete
    suspend fun delete(addressFile: AddressFile)

    @Query("SELECT * FROM File_Address")
     suspend fun getAllFiles() : List<AddressFile>
}
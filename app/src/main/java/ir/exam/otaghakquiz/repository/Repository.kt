package ir.exam.otaghakquiz.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ir.exam.otaghakquiz.db.AddressFile
import ir.exam.otaghakquiz.db.GalleryDatabase

class Repository (context: Context) {
    private var database: GalleryDatabase = GalleryDatabase.getGalleryDatabase(context)!!
    private var list =MutableLiveData<List<AddressFile>>()
    suspend fun insert(addressFile: AddressFile){
        database.fileAddressDao().insert(addressFile)
    }

    suspend fun delete(addressFile: AddressFile){
        database.fileAddressDao().delete(addressFile)
    }

    suspend fun getImagesUrlList():List<AddressFile>{
        var x = database.fileAddressDao().getAllFiles()
        return database.fileAddressDao().getAllFiles()
    }
}